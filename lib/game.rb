require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'

class Game

  attr_accessor :board, :player1, :player2, :current_player, :name, :mark

  def initialize(player1, player2)
    @board = Board.new
    @player1 = player1
    @player2 = player2
    player1.mark = :X
    player2.mark = :O
    @name = name
    @current_player = player1
  end

  def switch_players!
    if @current_player == @player1
      @current_player = @player2
    else
      @current_player = @player1
    end
  end

  def play_turn
    @current_player.display(board)
    move = @current_player.get_move
    board.place_mark(move, current_player.mark)
    switch_players!
  end

  def play
    until @board.over?
      play_turn
    end
  end

end


  if __FILE__ ==$PROGRAM_NAME
    player1 = HumanPlayer.new
    player2 = ComputerPlayer.new
    this_game = Game.new(player1, player2)
    this_game.play
  end
