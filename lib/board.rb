class Board
  attr_accessor :grid, :pos

  def initialize(grid=[[nil, nil, nil], [nil, nil, nil], [nil, nil, nil]])
    @grid = grid
  end

  def place_mark(pos, mark)
    pos1 = pos[0]
    pos2 = pos[1]
    if mark == nil
      grid[pos1][pos2] = nil
      return nil
    end
    grid[pos1][pos2] = mark.to_sym
  end

  def empty?(pos)
    pos1 = pos[0]
    pos2 = pos[1]
    if grid[pos1][pos2] == nil
      return true
    else
      return false
    end
  end

  def winner
     return grid[0][0] if grid[0][0] == grid[0][1] && grid[0][1] == grid[0][2] && grid[0][0]
     return grid[0][1] if grid[0][1] == grid[1][1] && grid[1][1] == grid[2][1] && grid[0][1]
     return grid[0][2] if grid[0][2] == grid[1][2] && grid[1][2] == grid[2][2] && grid[0][2]
     return grid[1][0] if grid[1][0] == grid[1][1] && grid[1][1] == grid[1][2] && grid[1][0]
     return grid[1][1] if grid[1][1] == grid[0][0] && grid[0][0] == grid[2][2] && grid[1][1]
     return grid[2][0] if grid[2][0] == grid[2][1] && grid[2][1] == grid[2][2] && grid[2][0]
     return grid[0][0] if grid[0][0] == grid[1][0] && grid[1][0] == grid[2][0] && grid[0][0]
     return grid[0][2] if grid[0][2] == grid[1][1] && grid[1][1] == grid[2][0] && grid[0][2]
    nil
  end


  def over?
    if winner
      return true
    else
      return true unless grid.flatten.include?(nil)
    end
  end

end


# #:X is winner
# return :X if grid[0][0] == :X && grid[0][1] == :X && grid[0][2] == :X
# return :X if grid[1][0] == :X && grid[1][1] == :X && grid[1][2] == :X
# return :X if grid[2][0] == :X && grid[2][1] == :X && grid[2][2] == :X
# return :X if grid[0][0] == :X && grid[1][0] == :X && grid[2][0] == :X
# return :X if grid[0][1] == :X && grid[1][1] == :X && grid[2][1] == :X
# return :X if grid[0][2] == :X && grid[1][2] == :X && grid[2][2] == :X
# return :X if grid[0][0] == :X && grid[1][1] == :X && grid[2][2] == :X
# return :X if grid[2][0] == :X && grid[1][1] == :X && grid[0][2] == :X
# #:O is winner
# return :O if grid[0][0] == :O && grid[0][1] == :O && grid[0][2] == :O
# return :O if grid[1][0] == :O && grid[1][1] == :O && grid[1][2] == :O
# return :O if grid[2][0] == :O && grid[2][1] == :O && grid[2][2] == :O
# return :O if grid[0][0] == :O && grid[1][0] == :O && grid[2][0] == :O
# return :O if grid[0][1] == :O && grid[1][1] == :O && grid[2][1] == :O
# return :O if grid[0][2] == :O && grid[1][2] == :O && grid[2][2] == :O
# return :O if grid[0][0] == :O && grid[1][1] == :O && grid[2][2] == :O
# return :O if grid[2][0] == :O && grid[1][1] == :O && grid[0][2] == :O

# return grid[0][0] if grid[0][0] == grid[0][1] && grid[0][1] == grid[0][2] && grid[0][0]
# return grid[0][1] if grid[0][0] == grid[0][1] && grid[0][1] == grid[0][2] && grid[0][0]
#
#
# return grid[0][2] if (grid[0][0] && grid[0][1]) || (grid[1][2] && grid[2][2]) || (grid[1][1] && grid[2][0])
# return grid[1][0] if (grid[0][0] && grid[2][0]) || (grid[1][1] && grid[1][2])
# return grid[1][1] if (grid[0][0] && grid[2][2]) || (grid[2][0] && grid[2][2]) || (grid[0][1] && grid[2][1]) || (grid[1][0] && grid[1][2])
# return grid[1][2] if (grid[1][0] && grid[1][1]) || (grid[0][2] && grid[2][2])
# return grid[2][0] if (grid[2][1] && grid[2][2]) || (grid[0][0] && grid[1][0]) || (grid[1][1] && grid[0][2])
# return grid[2][1] if (grid[2][0] && grid[2][2]) || (grid[1][1] && grid[0][1])
# return grid[2][2] if (grid[0][0] && grid[1][1]) || (grid[0][2] && grid[1][2]) || (grid[2][0] && grid[2][1])
