class HumanPlayer
  attr_accessor :name, :board, :mark

  def initialize(name="I am the human.")
    @name = name
    @board = board
  end

  def display(board)
   @board = board
   print board.grid
 end

  def get_move
    puts "Where would you like to make a mark?"
    human_move = gets.chomp

    move_array = []
    human_move.split.each do |el|
      move_array << el.to_i
    end
    move_array
  end

end
