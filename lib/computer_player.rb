class ComputerPlayer
  attr_accessor :name, :board, :mark

  def initialize(name="I am the computer.")
    @name = name
    @board = board
  end

  def display(board)
    @board = board
  end

  def get_move
    empty_spaces = []
    @board.grid.each_with_index do |row, i1|
      row.each_with_index do |column, i2|
        if column.nil?
          empty_spaces << [i1, i2]
        end
      end
    end

     empty_spaces.each do |el|
       @board.place_mark(el, :O)
       p @board
       if !!@board.winner == true
         return el
       else
         @board.place_mark(el, nil)
       end
     end

     empty_spaces.each do |el|
       @board.place_mark(el, :X)
       if !!@board.winner == true
         @board.place_mark(el, nil)
         @board.place_mark(el, :O)
         return el
       else
         @board.place_mark(el, nil)
       end
     end


     empty_spaces.sample

  end

end
